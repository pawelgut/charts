package standaryzacja;

import java.util.Date;

public class GetInBankStandard {
    Date dataTransakcji;
    Date dataKsiegowania;
    String opisTransakcji;
    String miejsce;
    double kwotaTransakcji;
    double saldoPoOperacji;

    public GetInBankStandard(Date dataTransakcji, Date dataKsiegowania, String opisTransakcji, String miejsce, double kwotaTransakcji, double saldoPoOperacji) {
        this.dataTransakcji = dataTransakcji;
        this.dataKsiegowania = dataKsiegowania;
        this.opisTransakcji = opisTransakcji;
        this.miejsce = miejsce;
        this.kwotaTransakcji = kwotaTransakcji;
        this.saldoPoOperacji = saldoPoOperacji;
    }

    public String getMiejsce() {
        return miejsce;
    }

    public void setMiejsce(String miejsce) {
        this.miejsce = miejsce;
    }

    public Date getDataTransakcji() {
        return dataTransakcji;
    }

    public void setDataTransakcji(Date dataTransakcji) {
        this.dataTransakcji = dataTransakcji;
    }

    public Date getDataKsiegowania() {
        return dataKsiegowania;
    }

    public void setDataKsiegowania(Date dataKsiegowania) {
        this.dataKsiegowania = dataKsiegowania;
    }

    public String getOpisTransakcji() {
        return opisTransakcji;
    }

    public void setOpisTransakcji(String opisTransakcji) {
        this.opisTransakcji = opisTransakcji;
    }

    public double getKwotaTransakcji() {
        return kwotaTransakcji;
    }

    public void setKwotaTransakcji(double kwotaTransakcji) {
        this.kwotaTransakcji = kwotaTransakcji;
    }

    public double getSaldoPoOperacji() {
        return saldoPoOperacji;
    }

    public void setSaldoPoOperacji(double saldoPoOperacji) {
        this.saldoPoOperacji = saldoPoOperacji;
    }
}
