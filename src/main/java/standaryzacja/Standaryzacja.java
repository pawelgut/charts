package standaryzacja;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Standaryzacja {
    public static ArrayList<GetInBankStandard> stringToGetInBankStandard(ArrayList<String> inputLines) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        ArrayList<GetInBankStandard> transakcja = new ArrayList<GetInBankStandard>();
        Date dataTransakcji;
        Date dataKsiegowania;
        String opis, miejsce;
        double kwotaTransakcji=0, saldoPoOperacji=0;
        String reszta;
        String[] podzial;
        boolean czyKsiegowane;
        char znak='P';
        char znak2 = 'O';
        int pomocnicza=0;
        int j;
        for(String line : inputLines) {
            kwotaTransakcji=0;
            saldoPoOperacji=0;
            miejsce=null;
            dataKsiegowania=null;
            dataTransakcji=null;
            opis=null;

            System.out.println(line);

            dataTransakcji = format.parse(line.substring(0,10));
            czyKsiegowane = false;
            try {
                String test = Character.toString(line.charAt(11));
                Integer.parseInt(test);
                czyKsiegowane=true;
                j=22;
            }catch (NumberFormatException e) {
                czyKsiegowane=false;
                j=11;
            }
            if (czyKsiegowane) {
                dataKsiegowania = format.parse(line.substring(11,21));
            }
            else {
                dataKsiegowania=null;
            }
            reszta = line.substring(j);
            podzial = reszta.split(",");
            opis = podzial[0];
            miejsce = podzial[1] + podzial[2].substring(0,3);

            if (czyKsiegowane) {
                for(int h=0 ; h<podzial[3].length() ; h++) {
                    if (podzial[3].charAt(h) == znak) {
                        if (podzial[3].charAt(h+1) == znak2) {
                            pomocnicza=h+1;
                            break;
                        }
                        pomocnicza=h;
                        break;
                    }
                }
                reszta = podzial[2].substring(4) + "." + podzial[3].substring(0,pomocnicza);
                kwotaTransakcji = Double.parseDouble(reszta);

                reszta = podzial[3].substring(pomocnicza+4) + "." + podzial[4].substring(0,2);
                reszta = reszta.replaceAll("\\s+", "");
                saldoPoOperacji = Double.parseDouble(reszta);

            }
            if (dataKsiegowania != null) {
                transakcja.add(new GetInBankStandard(dataTransakcji, dataKsiegowania, opis, miejsce, kwotaTransakcji, saldoPoOperacji));
            }
        }
        return transakcja;
    }
}
