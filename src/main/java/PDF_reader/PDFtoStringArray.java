package PDF_reader;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class PDFtoStringArray {

    ArrayList<String> rows;

    public PDFtoStringArray(String PDFname) throws IOException{
        //this.line = getLines(PDFname);
        this.rows = getImportantRows(PDFname);
    }

    private String[] getLinesFromPDF(String PDFfileName) throws IOException {

        PDDocument document = PDDocument.load(new File(PDFfileName));
        document.getClass();
        String lines[]=null;

        if (!document.isEncrypted()) {

            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition(true);
            PDFTextStripper tStripper = new PDFTextStripper();
            String pdfFileInText = tStripper.getText(document);
            // split by whitespace
            lines = pdfFileInText.split("\\r?\\n");
        }
        document.close();
        return lines;

    }

    private ArrayList<String> getImportantRows(String PDFfileName) throws IOException {
        String[] lines = getLinesFromPDF(PDFfileName);
        ArrayList<String> outLines = new ArrayList<String>();
        boolean isImportant;

        for(String line : lines ) {
            isImportant = false;
            try {
                String test = Character.toString(line.charAt(0));
                Integer.parseInt(test);
                isImportant=true;
            }catch (NumberFormatException e) {
                isImportant=false;
            }
            if (isImportant) {
                outLines.add(line);
            }
        }
        outLines.remove(0);
        return outLines;
    }

    public void printLines() {
        for(String line : rows) {
            System.out.println(line);
        }
    }

    public ArrayList<String> getRows() {
        return rows;
    }

}
