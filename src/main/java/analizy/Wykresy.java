package analizy;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import standaryzacja.GetInBankStandard;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Wykresy extends JFrame  {
    public Wykresy(ArrayList<GetInBankStandard> transakcje, String title) throws IOException{
        super(title);
        // Create dataset
        DefaultCategoryDataset dataset = createDataset(transakcje);
        // Create chart
        JFreeChart chart = ChartFactory.createLineChart(
                "Site Traffic (WWW.BORAJI.COM)", // Chart title
                "Date", // X-Axis Label
                "Number of Visitor", // Y-Axis Label
                dataset
        );

        ChartPanel panel = new ChartPanel(chart);
        setContentPane(panel);
        File file = new File("chart.png");
        ChartUtilities.saveChartAsPNG(file, chart, 500, 500);

    }

    private DefaultCategoryDataset createDataset(ArrayList<GetInBankStandard> transakcje) {

        String series1 = "Saldo";
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        for(GetInBankStandard transakcja : transakcje) {
            dataset.addValue(transakcja.getSaldoPoOperacji(), series1, dateFormat.format(transakcja.getDataTransakcji()));
        }

        return dataset;
    }
}
