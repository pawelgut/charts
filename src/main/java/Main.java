import PDF_reader.PDFtoStringArray;
import analizy.Wykresy;
import standaryzacja.GetInBankStandard;
import standaryzacja.Standaryzacja;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws IOException, ParseException {
        PDFtoStringArray pdFtoStringArray = new PDFtoStringArray("Historia_karty.pdf");
        pdFtoStringArray.printLines();
        ArrayList<GetInBankStandard> transakcje = Standaryzacja.stringToGetInBankStandard(pdFtoStringArray.getRows());

        Wykresy wykresy = new Wykresy(transakcje, "TRANSAKCJE");

//        for(GetInBankStandard transakcja : transakcje) {
//            System.out.println("data tr:"+transakcja.getDataTransakcji()+"data ks:"+transakcja.getDataKsiegowania()+
//                    "opis:"+transakcja.getOpisTransakcji()+"miejsce"+transakcja.getMiejsce()+"kwota:"+transakcja.getKwotaTransakcji()+
//                    "saldo:"+transakcja.getSaldoPoOperacji());
//        }
    }
}
